﻿using System.Linq;

namespace Repository
{
    public interface IRepository<T> where T : class
    {
        IQueryable<T> Table();
        T Create(T item);
        T Update(T item);
        void Delete(T entity);
    }
}
