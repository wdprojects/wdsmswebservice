//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Repository
{
    using System;
    using System.Collections.Generic;
    
    public partial class Order
    {
        public int Id { get; set; }
        public string StoreName { get; set; }
        public string StoreDetails { get; set; }
        public string OrderNumber { get; set; }
        public string Message { get; set; }
        public Nullable<decimal> Price { get; set; }
        public string PaymentStatus { get; set; }
        public string Contact { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<bool> Responded { get; set; }
        public Nullable<bool> ReminderSent { get; set; }
        public string StoreMobile { get; set; }
        public string Status { get; set; }
    }
}
