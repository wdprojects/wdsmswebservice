﻿using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;

namespace Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {
        public static wdWebServiceSMSEntities Context { get; set; }

        public Repository()
        {
            Context = new wdWebServiceSMSEntities();
        }

        public IQueryable<T> Table()
        {
            return Context.Set<T>();
        }

        public T Create(T item)
        {
            try
            {
                item = Context.Set<T>().Add(item);
                Context.SaveChanges();
                return item;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public T Update(T item)
        {
            try
            {
                Context.Set<T>().AddOrUpdate(item);
                Context.SaveChanges();
                return item;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public void Delete(T entity)
        {
            try
            {
                Context.Set<T>().Remove(entity);
                Context.SaveChanges();
            }
            catch
            {

            }
        }
       
    }
}
