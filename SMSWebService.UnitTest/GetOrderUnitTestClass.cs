﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Repository;
using SMSWebService.UnitTest.SMSService;
using WDSMSWebService1;
using WDSMSWebService1.Misc;

namespace SMSWebService.UnitTest
{
    [TestClass]
    public class GetOrderUnitTestClass
    {
        [TestMethod]
        public void GetOrdersUnitTestGetOrderByDoimainName()
        {
            //Arrange
            const string websiteAddress = "demo@imenuweb.co.uk";
            //Act
            List<WebsiteOrder> result = OrderFunctions.GetOrdersByDomain(websiteAddress, -34);
            var hasOrder = result.FirstOrDefault(x => x.OrderNumber =="41578" );
            //Assert
            Assert.IsNotNull(hasOrder);
        }


        [TestMethod]
        public void GetOrdersUnitTestGetDomainBodyFromWebsiteAddress()
        {
            //Arrange
            const string websiteAddress = "imenuweb.co.uk";
            //Act
            string result = OrderFunctions.GetDomainBody(websiteAddress);
            const string expectedResult = "imenuweb";
            //Assert
            Assert.AreEqual(expectedResult,result);
        }

        [TestMethod]
        public void GetOrdersUnitTestFromWebService()
        {
            const string websiteAddress = "demo@imenuweb.co.uk";
            //Act
            SMSService.Service1Client s = new Service1Client();
            var orders = s.GetWebsiteOrdersByDomain(websiteAddress);
            var hasOrder = orders.FirstOrDefault(x => x.OrderNumber == "41578");
            //Assert
            Assert.IsNotNull(hasOrder);
        }



        //[TestMethod]
        //public void GetOrderUnitTest()
        //{
        //    //Arrange

        //    //Act

        //    //Assert
        //}
    }

}
