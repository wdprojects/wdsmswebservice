﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Repository;
using WDSMSWebService1.Misc;

namespace WDSMSWebService1
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        //public string GetData(int value)
        //{
        //    return string.Format("You entered: {0}", value);
        //}

        //public CompositeType GetDataUsingDataContract(CompositeType composite)
        //{
        //    if (composite == null)
        //    {
        //        throw new ArgumentNullException("composite");
        //    }
        //    if (composite.BoolValue)
        //    {
        //        composite.StringValue += "Suffix";
        //    }
        //    return composite;
        //}

        private readonly IRepository<Setting> _settingRepository = new Repository<Setting>();
        private readonly IRepository<Customer> _customerRepository = new Repository<Customer>();
        private readonly IRepository<MessageLog> _messageRepository = new Repository<MessageLog>();
        private readonly IRepository<Order> _orderRepository = new Repository<Order>();

        RouteSMSServiceWrapper smsServiceWrapper = new RouteSMSServiceWrapper();

        public bool CheckToken(string token)
        {
            string Token = _settingRepository.Table().FirstOrDefault(x => x.SettingName == "Token") == null ? "" : _settingRepository.Table().FirstOrDefault(x => x.SettingName == "Token").SettingValue;
            if (string.IsNullOrEmpty(token))
            {
                return false;
            }
            return Token == token;
        }

        public int GetCredits(string customerName)
        {
            var customer = _customerRepository.Table().FirstOrDefault(x => x.CustomerName == customerName);
            int credits = (int)(customer == null ? 0 : customer.Credits);
            return credits;
        }


        public string SendSingleSMS(string userId, string senderName, string destination, string message, string token)
        {
            try
            {
                var user = _customerRepository.Table().FirstOrDefault(x => x.CustomerName == userId);
                var result = "";
                if (user.Credits == 0)
                {
                    return "Insufficient Funds, please top up to continue";
                }
                if (user.Credits > 0 && CheckToken(token))
                {
                    result = smsServiceWrapper.SendSingleSMS(senderName, destination, message);
                }

                var resultSegments = result.Split('|');
                var resultCode = resultSegments[0];
                if (resultCode == "1701")
                {
                    //deduct credit by 1
                    DeductCreditBy1(userId);
                }
                var msg = new MessageLog
                {
                    UserId = user.Id,
                    Date = DateTime.Now,
                    Destination = destination,
                    MessageBody = message,
                    Messagetype = "SingleSMS",
                    SenderName = senderName,
                    MessageResponse = resultCode,
                    Success = resultCode == "1701"
                };


                _messageRepository.Update(msg);
                return result;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }

        }

        public string SendMobileSMSNotification(string userId, string senderName, string destination, string message, string token, string messageType)
        {
            try
            {
                var user = _customerRepository.Table().FirstOrDefault(x => x.CustomerName == userId);
                var result = "";

                if (CheckToken(token) && user != null && user.NotificationsPaid)
                {
                    result = smsServiceWrapper.SendSingleSMS(senderName, destination, message);
                }

                var resultSegments = result.Split('|');
                var resultCode = resultSegments[0];

                //Record message in log file
                var msg = new MessageLog
                {
                    UserId = user.Id,
                    Date = DateTime.Now,
                    Destination = destination,
                    MessageBody = message,
                    Messagetype = messageType,
                    SenderName = senderName,
                    MessageResponse = resultCode,
                    Success = resultCode == "1701"
                };

                _messageRepository.Update(msg);

                return result;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        private void InsertMessageIntoLog(MessageLog msg)
        {
            try
            {
                _messageRepository.Update(msg);
            }
            catch (Exception ex)
            {
                //
            }

        }

        private void DeductCreditBy1(string userId)
        {
            var user = _customerRepository.Table().FirstOrDefault(x => x.CustomerName == userId);
            if (user != null && user.Credits != 0)
            {
                user.Credits--;
                _customerRepository.Update(user);
            }
        }


        public string GetBalance()
        {
            return smsServiceWrapper.GetBalance();
        }



        public bool AddCredit(int creditAmount, string userId, string token)
        {
            try
            {
                var user = _customerRepository.Table().FirstOrDefault(x => x.CustomerName == userId);
                if (user != null && CheckToken(token))
                {
                    user.Credits += creditAmount;
                    _customerRepository.Update(user);
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool LogOrder(string storeName, string storeDetails, string orderNumber, string message, decimal price, string paymentStatus, string contact, string storeMobileNumber)
        {
            try
            {
                _orderRepository.Update(new Order
                {
                    StoreName = storeName,
                    StoreDetails = storeDetails,
                    OrderNumber = orderNumber,
                    Message = message,
                    Price = price,
                    PaymentStatus = paymentStatus,
                    Contact = contact,
                    Responded = false, // When order has been accepted from mobile app
                    Date = DateTime.Now,
                    ReminderSent = false, // When Sheduled task has found an order that has not been actioned
                    StoreMobile = storeMobileNumber
                });
            }
            catch (Exception)
            {
                return false;
            }
            
            return true;
        }


        public bool UpdateAcceptanceStatus(string orderId, string status, string storeId)
        {
            try
            {
                var order = _orderRepository.Table().FirstOrDefault(x => x.OrderNumber == orderId && x.StoreName==storeId);
                if (order != null)
                {
                    order.Responded = true;
                    order.Status = status;
                }
                _orderRepository.Update(order);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public bool UpdateReminderSentStatus(string orderId)
        {
            try
            {
                var order = _orderRepository.Table().FirstOrDefault(x => x.OrderNumber == orderId);
                if (order != null) order.ReminderSent = true;
                _orderRepository.Update(order);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public List<WebsiteOrder> GetWebsiteOrdersByDomain(string DomainName)
        {
            return OrderFunctions.GetOrdersByDomain(DomainName);
        }
    }
}
