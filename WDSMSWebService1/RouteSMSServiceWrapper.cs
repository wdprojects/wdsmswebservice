﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Repository;
using WDSMSWebService1.RouteSMSService;

namespace WDSMSWebService1
{
    public class RouteSMSServiceWrapper
    {
        private RouteSMSService.ServiceClient SMSService;
        private string _userName;
        private string _password;
        public RouteSMSServiceWrapper()
        {
            SMSService = new ServiceClient();
            _userName = _settingRepository.Table().FirstOrDefault(x => x.SettingName == "UserName").SettingValue;
            _password = _settingRepository.Table().FirstOrDefault(x => x.SettingName == "Password").SettingValue;
        }

        private readonly IRepository<Setting> _settingRepository = new Repository<Setting>();


        public string SendSingleSMS(string SenderName, string Destination, string Message)
        {
            if (!string.IsNullOrEmpty(_userName) && !string.IsNullOrEmpty(_password))
            {
                return SMSService.sendSingleMessage(_userName, _password, SenderName, Destination, Message, "0", "");
            }
            return "Message not sent";
        }

        public string GetBalance()
        {
            return SMSService.getCreditDetails(_userName, _password);
        }
    }
}