﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WDSMSWebService1
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {

        //[OperationContract]
        //string GetData(int value);

        //[OperationContract]
        //CompositeType GetDataUsingDataContract(CompositeType composite);
        //// TODO: Add your service operations here

        [OperationContract]
        bool CheckToken(string token);

        [OperationContract]
        int GetCredits(string customerName);

        [OperationContract]
        string GetBalance();

        [OperationContract]
        string SendSingleSMS(string userId, string senderName, string destination, string message, string token);

        [OperationContract]
        string SendMobileSMSNotification(string userId, string senderName, string destination, string message, string token, string messageType);

        [OperationContract]
        bool AddCredit(int creditAmount, string userId, string token);

        [OperationContract]
        bool LogOrder(string storeName, string storeDetails, string orderNumber, string message, decimal price, string paymentStatus, string contact, string storeMobileNumber);

        [OperationContract]
        bool UpdateAcceptanceStatus(string orderId, string status, string storeId);//Store id is the email address

        [OperationContract]
        bool UpdateReminderSentStatus(string orderId);

        [OperationContract]
        List<WebsiteOrder> GetWebsiteOrdersByDomain(string DomainName);
    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }
    [DataContract]
    public class WebsiteOrder
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string StoreName { get; set; }
        [DataMember]
        public string StoreDetails { get; set; }
        [DataMember]
        public string OrderNumber { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public Nullable<decimal> Price { get; set; }
        [DataMember]
        public string PaymentStatus { get; set; }
        [DataMember]
        public string Contact { get; set; }
        [DataMember]
        public Nullable<System.DateTime> Date { get; set; }
        [DataMember]
        public Nullable<bool> Responded { get; set; }
        [DataMember]
        public Nullable<bool> ReminderSent { get; set; }
        [DataMember]
        public string StoreMobile { get; set; }
        [DataMember]
        public string Status { get; set; }


    }
}
