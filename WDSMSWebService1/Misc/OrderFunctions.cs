﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Repository;

namespace WDSMSWebService1.Misc
{
    public class OrderFunctions
    {
        static readonly IRepository<Order> OrderRepository = new Repository<Order>();

        public static List<WebsiteOrder> GetOrdersByDomain(string websiteAddress, int hours=-6)
        {
            try
            {
                DateTime timeLimit = hours != -6 ? DateTime.Now.AddHours(hours) : DateTime.Now.AddHours(-6);
                string domainBody = GetDomainBody(websiteAddress);
                if (string.IsNullOrEmpty(domainBody))return new List<WebsiteOrder>();

                var orders = OrderRepository.Table().Where(x => x.StoreName.Contains(domainBody) && x.Date > timeLimit && x.Responded == false).ToList();
                List<WebsiteOrder> websiteOrdersList = new List<WebsiteOrder>();
                if (orders.Any())
                {
                    websiteOrdersList.AddRange(orders.Select(order => new WebsiteOrder
                    {
                        Contact = order.Contact,
                        Message = order.Message,
                        Date = order.Date,
                        Id = order.Id,
                        OrderNumber = order.OrderNumber,
                        PaymentStatus = order.PaymentStatus,
                        Price = order.Price,
                        ReminderSent = order.ReminderSent,
                        Responded = order.Responded,
                        Status = order.Status,
                        StoreDetails = order.StoreDetails,
                        StoreMobile = order.StoreMobile,
                        StoreName = order.StoreName
                    }));
                }
                return websiteOrdersList;
            }
            catch (Exception)
            {
               return new List<WebsiteOrder>();
            }
        }

        public static string GetDomainBody(string websiteAddress)
        {
            try
            {
                string[] step1 = websiteAddress.Split('.');
                return step1[0];
                
            }
            catch (Exception)
            {
                return "";
            }
           
        }


    }
}