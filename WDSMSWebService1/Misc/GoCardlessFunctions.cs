﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using GoCardless;


namespace WDSMSWebService1.Misc
{
    public class GoCardlessFunctions
    {
        public static async void GetClientList(GoCardlessClient client)
        {
            
            var listResponse = await client.Customers.ListAsync();
            var customers = listResponse.Customers;
            Console.WriteLine("Customers: " + string.Join(", ", customers.Select(c => c.Id)));
        }

        
    }
}